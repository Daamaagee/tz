<?php

require_once 'ImageInterface.php';


class Image implements ImageInterface
{
    private $tmp_file;

    function __construct($tmp_file)
    {
        $this->tmp_file = $tmp_file;
    }

    public function open($filename)
    {
        return $filename;
    }

    public function getWidth()
    {
        return getimagesize($this->tmp_file)[0];
    }

    public function getHeight()
    {
        return getimagesize($this->tmp_file)[1];
    }

    public function getType()
    {
        $path = $_FILES['image']['name'];
        return pathinfo($path, PATHINFO_EXTENSION);

    }

    public function isImage()
    {
        $possible_types = ['jpg', 'jpeg', 'png'];
        $path = $this->getType();

        if (!in_array($path, $possible_types)) {
            return 'FAlSE';
        }
        return 'TRUE';

    }

    public function save($filepath, $filename, $image_type = IMAGETYPE_JPEG)
    {
        move_uploaded_file($filepath, './images/' . $filename . "." . $image_type);

    }

}