CREATE TABLE `test`.`images` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`original_name` VARCHAR(50) NOT NULL,
	`create_datetime` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `test`.`image_new_name` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `new_name` VARCHAR(50) NOT NULL ,
  `id_original_name` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE = InnoDB;