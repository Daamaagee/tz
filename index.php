<?php
require_once 'Controller.php';
require_once 'Image.php';

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Test</title>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script>

        $(document).ready(function () {

            $(function () {
                $('#fileForm').on('submit', function (e) {
                    e.preventDefault();
                    var $that = $(this),
                        formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму
                    $.ajax({
                        url: $that.attr('action'),
                        type: $that.attr('method'),
                        contentType: false, // важно - убираем форматирование данных по умолчанию
                        processData: false, // важно - убираем преобразование строк по умолчанию
                        data: formData,
                        dataType: 'json',
                        success: function (json) {
                            if (json) {
                                $that.replaceWith(json);
                            }
                        }
                    });
                });
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#getTable').click(function () {
                $.get('GetTable.php', {id: 2}, function (data) {
                    $('#output').html(data);
                });
            });
        });
    </script>

    <style>
        .wrapper {
            text-align: center;
            margin: 2em;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>

</head>
<body>


<!-- Create Form HERE -->
<div class="wrapper">
    <form id="fileForm" action="Controller.php" method="post" enctype="multipart/form-data">
        <p>Изображения:
            <input type="file" name="image"/>
            <input id="sumbit" type="submit" value="Отправить"/>
        </p>
    </form>
    <input id="getTable" type="submit" value="Получить данные">
</div>


<?php
//if (getImages() == 'FALSE'){
//    echo 'Нет данных';
//}
//else{
?>
<table id="output">
    <!--    <tr>-->
    <!--        <th>id</th>-->
    <!--        <th>original_name</th>-->
    <!--        <th>create_datetime</th>-->
    <!--    </tr>-->
    <!--    --><?php
    //
    //    foreach (getImages() as $image) {
    //        ?>
    <!--        <tr>-->
    <!--            <td>--><? //= $image['id'] ?><!--</td>-->
    <!--            <td><a href="/images/--><? //= $image['new_name'] ?><!--">-->
    <? //= $image['original_name'] ?><!--</a></td>-->
    <!--            <td>--><? //= $image['create_datetime'] ?><!--</td>-->
    <!--        </tr>-->
    <!---->
    <!--        --><?php
    //    }
    //    }
    ?>


</table>


</body>

</html>