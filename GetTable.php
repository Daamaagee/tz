<?php
require_once 'Database.php';

$db = Database::getInstance();
$mysqli = $db->getConnection();
$query = "SELECT images.id,images.original_name,images.create_datetime,image_new_name.new_name FROM images RIGHT JOIN image_new_name ON image_new_name.id=images.id";
$result = $mysqli->query($query);
if ($result->num_rows > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $images[] = $row;
    }
    echo '?>
        <tr>
        <th>id</th>
        <th>original_name</th>
        <th>create_datetime</th>
    </tr>
    <?php';

    foreach ($images as $image) {
        ?>
        <tr>
            <td><?= $image['id'] ?></td>
            <td><a href="/images/<?= $image['new_name'] ?>"><?= $image['original_name'] ?></a></td>
            <td><?= $image['create_datetime'] ?></td>
        </tr>

        <?php

    }
    ?>


    <?php
} else {
    echo '<center>Нет данных</center>';
}