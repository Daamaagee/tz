<?php

/**
 * Created by PhpStorm.
 * User: Daamaagee
 * Date: 10.02.2017
 * Time: 18:05
 */
/*
* Mysql database class - only one connection alowed
*/

class Database
{
    private $_connection;
    private static $_instance; //The single instance
    private $_host = "127.0.0.1";
    private $_username = "root";
    private $_password = "";
    private $_database = "test";

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    private function __construct()
    {
        $this->_connection = new mysqli($this->_host, $this->_username,
            $this->_password, $this->_database);

        // Error handling
        if (mysqli_connect_error()) {
            trigger_error("Failed to conencto to MySQL: " . mysqli_connect_error(),
                E_USER_ERROR);
        }
    }

    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {
    }

    // Get mysqli connection
    public function getConnection()
    {
        return $this->_connection;
    }

    public function getId($table)
    {
        $query = "SELECT `id` FROM `$table`";
        $id = $this->_connection->query($query)->num_rows;
        return $id;
    }

    public function InsertOriginName($original_name)
    {
        $query = "INSERT INTO `images` (`original_name`, `create_datetime`) VALUES ('$original_name', NOW())";
        $this->_connection->query($query);

    }

    public function InsertNewName($new_name, $type, $id)
    {
        $temp = $new_name . '.' . $type;
        $query = "INSERT INTO `image_new_name` (`new_name`, `id_original_name`) VALUES ('$temp', '$id')";
        $this->_connection->query($query);

    }


}

