<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ajax загрузка файлов</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <style type="text/css">
        #add_field {
            display: inline-block;
            padding: 3px 7px;
            border: 1px solid #999;
            border-radius: 5px;
            box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }
    </style>
    <script>
        $(function () {
            $('#my_form').on('submit', function (e) {
                e.preventDefault();
                var $that = $(this),
                    formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму
                $.ajax({
                    url: $that.attr('action'),
                    type: $that.attr('method'),
                    contentType: false, // важно - убираем форматирование данных по умолчанию
                    processData: false, // важно - убираем преобразование строк по умолчанию
                    data: formData,
                    dataType: 'json',
                    success: function (json) {
                        if (json) {
                            $that.replaceWith(json);
                        }
                    }
                });
            });
        });
    </script>
</head>

<body>
<form action="Controller.php" method="post" id="my_form" enctype="multipart/form-data">
    <label for="avatar">Выберите файлы для загрузки:</label><br>
    <input type="file" name="image">
    <div id="additional_fields"></div>
    <p>
        <input type="submit" id="submit" value="Отправить">
    </p>
</form>
</body>
</html>