<?php

/**
 * Create magic here...
 */

require_once 'Image.php';
require_once 'Database.php';

$db = Database::getInstance();
$mysqli = $db->getConnection();


if (isset($_FILES['image'])) {
    $req = false;
    ob_start();
    $image = new Image($_FILES['image']['tmp_name']);

    if ($image->isImage() == 'TRUE') {
        $width = $image->getWidth();
        $height = $image->getHeight();
        $original_name = basename($_FILES["image"]["name"]);
        $db->InsertOriginName($original_name);
        $id = $db->getId('images');
        $new_name = $id . "." . $width . "x" . $height;
        $db->InsertNewName($new_name, $image->getType(), $id);
        $image->save($_FILES['image']['tmp_name'], $new_name, $image->getType());
        echo "Загруженно";

    } else {
        echo "Неверный формат файла";
    }
    $req = ob_get_contents();
    ob_end_clean();
    echo json_encode($req); // вернем полученное в ответе
    exit;
}


function getImages()
{
    $db = Database::getInstance();
    $mysqli = $db->getConnection();
    $query = "SELECT images.id,images.original_name,images.create_datetime,image_new_name.new_name FROM images RIGHT JOIN image_new_name ON image_new_name.id=images.id";
    $result = $mysqli->query($query);
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $images[] = $row;
        }
        return $images;
    } else {
        return 'FALSE';
    }
}

